using DataLibrary;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Data
{
    public class DataService
    {
        public DataService() {
           // fb.writeData();
        }
       static string cn_ = "ServerType=1;User=SYSDBA;" +
                 @"Password=masterkey;Dialect=3;Database=fire_db.fdb;ClientLibrary=d:\fb\fbembed.dll";

        FireDb fb = new FireDb(cn_);
        
        public Task<Entity[]> GetForecastAsync(DateTime startDate)
        {
            fb.createDbFill(cn_);
            var temp = fb.getData("select * from forecast", 3);
           return Task.FromResult( temp.Select(t => new Entity { Date = t[0], city = t[1], TemperatureC = t[2] }).ToArray());
          
        }
    }
}
